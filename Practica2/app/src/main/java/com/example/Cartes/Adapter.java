package com.example.Cartes;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.CartesViewHolder> {
    private List<Carta> items;
    private List<Carta> historial;
    private Context context;
    int carta1;
    int cartaAnterior = -1;
    int contador = 0;

    public class CartesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView carta;

        public CartesViewHolder(View v) {
            super(v);
            carta = v.findViewById(R.id.cartaImg);
            carta.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            items.get(this.getAdapterPosition());
            final int value = this.getAdapterPosition();

            if (cartaAnterior == -1) {
                cartaAnterior = value;
                items.get(cartaAnterior).girar();
                notifyDataSetChanged();
            } else {
                items.get(value).girar();
                notifyDataSetChanged();

                if (items.get(value).getImage() == items.get(cartaAnterior).getImage()) {
                    items.get(value).setEstat(Carta.Estat.FIXED);
                    items.get(cartaAnterior).setEstat(Carta.Estat.FIXED);
                    notifyDataSetChanged();
                    cartaAnterior = -1;

                } else {


                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            items.get(value).girar();
                            items.get(cartaAnterior).girar();

                            notifyDataSetChanged();
                            cartaAnterior = -1;


                        }
                    }, 1000);

                }
            }

        }
    }

    public Adapter(List<Carta> items, Context context) {
        this.items = items;
        this.context = context;
    }


    @Override
    public CartesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.carta, parent, false);
        return new CartesViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CartesViewHolder holder, int position) {
        holder.carta.setImageResource(items.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


}

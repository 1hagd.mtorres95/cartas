package com.example.Cartes;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Iniciar cartes
        List<Carta> crt = new ArrayList<>();

        crt.add(new Carta(R.drawable.back,R.drawable.c0, Carta.Estat.BACK));
        crt.add(new Carta(R.drawable.back,R.drawable.c2, Carta.Estat.BACK));
        crt.add(new Carta(R.drawable.back,R.drawable.c3, Carta.Estat.BACK));

        crt.add(new Carta(R.drawable.back,R.drawable.c2, Carta.Estat.BACK));
        crt.add(new Carta(R.drawable.back,R.drawable.c3, Carta.Estat.BACK));
        crt.add(new Carta(R.drawable.back,R.drawable.c0, Carta.Estat.BACK));

        recycler = (RecyclerView) findViewById(R.id.CartesRecyclerView);
        recycler.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new GridLayoutManager(this, 3);

        // lManager = new GridLayotManager (this, 4);
        recycler.setLayoutManager(lManager);

        // Crear un nuevo adaptador
        adapter = new Adapter(crt, this);
        recycler.setAdapter(adapter);
    }
}

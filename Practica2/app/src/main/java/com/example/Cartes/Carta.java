package com.example.Cartes;

public class Carta {



    public enum Estat{BACK, FRONT, FIXED}
    private int backImage;
    private int frontImage;
    private Estat estat;

    public Carta(int backImage, int frontImage, Estat estat) {
        this.backImage = backImage;
        this.frontImage = frontImage;
        this.estat = estat;
    }

    public int getBackImage() {
        return backImage;
    }

    public int getFrontImage(){
        return frontImage;
    }



    public void setEstat(Estat estat) {
        this.estat = estat;
    }

    public Estat getEstat() {
        return estat;
    }

    public int getImage(){
        if (this.estat == Estat.BACK){
            return getBackImage();
        }else{
            return getFrontImage();
        }

    }


    public void girar() {

        if (this.getEstat() == Estat.BACK){
            this.setEstat(Estat.FRONT);
        }else{
            this.setEstat(Estat.BACK);
        }

    }
}
